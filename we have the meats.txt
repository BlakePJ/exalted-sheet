FUTURE FEATURES
- custom presets for rolling a a certain number of dice
- frosted-glass effect for dice roll-pull out
- make UI adjust to window resize better
- saving character sheet in real-time as fields are updated
- more comprehensive error testing
- send errors to log file

CHARACTER SHEET SAVE ORDER
1 - num bytes
v - character name
1 - num bytes
v - caste
1 - num bytes
v - supernal ability
2 - current xp
2 - total xp
2 - current solar xp
2 - total solar xp
1 - strength dots
1 - dex dots
1 - stamina dots
1 - charisma dots
1 - manipulation dots
1 - appearance row
1 - perception row
1 - intelligence row
1 - wits row
1 - essense dots