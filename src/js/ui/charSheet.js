const electron = require('electron');
const {ipcRenderer} = electron;
const { save, load } = require('../js/util/save.js');

// var color = '#8C8C8C';

let downloadButton = document.getElementById('download-button');
downloadButton.addEventListener('click', () => {
  save(document);
});

let loadButton = document.getElementById('load-button');
loadButton.addEventListener('click', () => {
  load();
});

let backButton = document.getElementById('back-button');
backButton.addEventListener('click', () => {
  ipcRenderer.send('back');
});

let rollHistArrow = document.getElementById('roll-history-arrow');
rollHistArrow.addEventListener('click', () => {
  let bar = document.getElementById('roll-history-bar');
  if(bar.style.right == '-300px') {
    bar.style.right = '0px';
    rollHistArrow.innerHTML = '&#x3009;';
  } else {
    bar.style.right = '-300px';
    rollHistArrow.innerHTML = '&#x3008;';
  }
});

let quickRollButton = document.getElementById('quick-roll-button');
quickRollButton.addEventListener('click', () => {
  const dice = document.getElementById('dice').value;
  const select = document.getElementById('doubles');
  const threshold = select.options[select.selectedIndex].id;
  roll(dice, threshold);

  // if(color == '#8C8C8C') {
  //   color = '#B3B3B3';
  // } else {
  //   color = '#8C8C8C';
  // }
});

const attrRollButtons = document.getElementsByClassName('attr-roll-button');
for(let i = 0; i < attrRollButtons.length; i++) {
  attrRollButtons[i].addEventListener('click', (event) => {
    ipcRenderer.send('show ability roll window', event.target.parentElement.parentElement.textContent.trim());
  });
}

const charmAddButton = document.getElementById('charm-add-button');
charmAddButton.addEventListener('click', () => {
  let newRow = document.getElementById('charms-table').insertRow();
  let newNameCell = newRow.insertCell();
  newNameCell.innerHTML += `<input type="text" class="charm-name">`;
  let newTypeCell = newRow.insertCell();
  newTypeCell.innerHTML += `<input type="text" class="charm-type">`;
  let newDurationCell = newRow.insertCell();
  newDurationCell.innerHTML += `<input type="text" class="charm-duration">`;
  let newCostCell = newRow.insertCell();
  newCostCell.innerHTML += `<input type="text" class="charm-cost">`;
  let newBookCell = newRow.insertCell();
  newBookCell.innerHTML += `<input type="text" class="charm-book">`;
  let newPageNumCell = newRow.insertCell();
  newPageNumCell.innerHTML += `<input type="text" class="charm-page">`;
  let newEffectCell = newRow.insertCell();
  newEffectCell.innerHTML += `<input type="text" class="charm-effect">`;
});

const weaponAddButton = document.getElementById('weapon-add-button');
weaponAddButton.addEventListener('click', () => {
  document.getElementById('weapons-table').innerHTML += 
        `<tr>
        <td><input type="text"></td>
        <td><input type="text"></td>
        <td><input type="text"></td>
        <td><input type="text"></td>
        <td><input type="text"></td>
        <td><input type="text"></td>
        <td><input type="text"></td>
        </tr>`;
});

const specialtiesAddButton = document.getElementById('specialties-add-button');
specialtiesAddButton.addEventListener('click', () => {
  document.getElementById('specialties-table').innerHTML += `<tr><td><input type="text"></td></tr>`;
});

const meritsAddButton = document.getElementById('merits-add-button');
meritsAddButton.addEventListener('click', () => {
  document.getElementById('merits-table').innerHTML += '<tr><td><input type="text"><tr><td>';
});

const limitTriggerAddButton = document.getElementById('limit-trigger-add-button');
limitTriggerAddButton.addEventListener('click', () => {
  document.getElementById('limit-trigger-table').innerHTML += '<tr><td><input type="text"><tr><td>';
});

const additionalAbilitiesAddButton = document.getElementById('additional-abilities-add-button');
additionalAbilitiesAddButton.addEventListener('click', () => {
  const regex = /[0-9]+/g;
  const text = document.getElementById('additional-abilities-table').innerHTML;
  let result;
  let highest = 1;
  while ((result = regex.exec(text)) != null) {
    highest = Number(result[0]);
  }
  console.log(highest);
  document.getElementById('additional-abilities-table').innerHTML += 
            `<tr>
              <td>
                <span class="checkbox">
                  <input type="checkbox" id="add-check-${highest + 1}">
                  <label for="add-check-${highest + 1}"></label>
                </span>
                <input type="text">
                <span class="checkbox">
                  <input type="checkbox" id="add-check-${highest + 2}">
                  <label for="add-check-${highest + 2}"></label>
                </span>
                <span class="checkbox">
                  <input type="checkbox" id="add-check-${highest + 3}">
                  <label for="add-check-${highest + 3}"></label>
                </span>
                <span class="checkbox">
                  <input type="checkbox" id="add-check-${highest + 4}">
                  <label for="add-check-${highest + 4}"></label>
                </span>
                <span class="checkbox">
                  <input type="checkbox" id="add-check-${highest + 5}">
                  <label for="add-check-${highest + 5}"></label>
                </span>
                <span class="checkbox">
                  <input type="checkbox" id="add-check-${highest + 6}">
                  <label for="add-check-${highest + 6}"></label>
                </span>
              </td>
            </tr>`;
});

const intimaciesAddButton = document.getElementById('intimacies-add-button');
intimaciesAddButton.addEventListener('click', () => {
  document.getElementById('intimacies-table').innerHTML += 
            `<tr>
              <td><input type="text"></td>
              <td><input type="text"></td>
            </tr>`;
});

const inventoryAddButton = document.getElementById('inventory-add-button');
inventoryAddButton.addEventListener('click', () => {
  document.getElementById('inventory-table').innerHTML += '<tr><td><input type="text"><td><tr>';
});

ipcRenderer.on('ability roll', (event, attr, abil) => {
  const attrDots = document.getElementById(`${attr}-row`).querySelectorAll('.checkbox');
  let attrCount = 0;
  for(let i = 0; i < attrDots.length; i++) {
    if(attrDots[i].getElementsByTagName('input')[0].checked == true) {
      attrCount += 1;
    }
  }
  // console.log(`attrCount = ${attrCount}`);

  if(abil == 'martial arts') {
    abil = 'martial-arts';
  }
  const abilDots = document.getElementById(`${abil}-row`).querySelectorAll('.checkbox');
  let abilCount = 0;
  for(let i = 0; i < abilDots.length; i++) {
    if(abilDots[i].getElementsByTagName('input')[0].checked == true) {
      abilCount += 1;
    }
  }
  // console.log(`abilCount = ${abilCount}`);

  roll(attrCount + abilCount);
});

function roll(dice, threshold=10) {
  const rolls = [];
  let hits = 0;

  for(let i = 0; i < dice; i++) {
    const roll = Math.floor(Math.random() * 10 + 1);
    rolls.push(roll);
    if(roll >= threshold) {
      hits += 2;
    } else if(roll >= 7) {
      hits += 1;
    }
  }
  displayRoll(rolls, hits);
}

function displayRoll(rolls, hits, threshold=10) {
  let output = `<li style="outline: none; display: inline-block; width: 18em; word-wrap: break-word; white-space: pre-line">Roll: `;
  for(let i = 0; i < rolls.length - 1; i++) {
    if(rolls[i] >= threshold) {
      output += '<span style="color: red">' + rolls[i] + '</span>';
    } else if(rolls[i] >= 7) {
      output += '<span style="color: #00cc00">' + rolls[i] + '</span>';
    } else {
      output += '<span style="color: grey">' + rolls[i] + '</span>';
    }

    output += '<span style="color: black"> + </span>';
  }

  if(rolls[rolls.length - 1] >= threshold) {
    output += '<span style="color: red">' + rolls[rolls.length - 1] + '</span>';
    // console.log('red');
  } else if(rolls[rolls.length - 1] >= 7) {
    output += '<span style="color: #00cc00">' + rolls[rolls.length - 1] + '</span>';
    // console.log('green');
  } else {
    output += '<span style="color: grey">' + rolls[rolls.length - 1] + '</span>';
    // console.log('grey');
  }

  const rollHistory = document.getElementById('roll-history');
  output += '\nHits: ' + hits;
  output += '</li>'
  output += '<li><hr style="border: 0; border-top: 1px solid black"></li>';
  rollHistory.innerHTML = output + rollHistory.innerHTML;
}