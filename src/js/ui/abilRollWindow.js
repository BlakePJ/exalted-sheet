const electron = require('electron');
const {ipcRenderer} = electron;

const abilSelect = document.getElementById('abil-select');
ipcRenderer.on('change ability', (event, ability) => {
  const options = abilSelect.options;
  for(let i = 0; i < abilSelect.length; i++) {
    if(options[i].innerHTML == ability) {
      abilSelect.selectedIndex = i;
      break;
    }
  }
});

const rollButton = document.getElementById('roll-button');
rollButton.addEventListener('click', () => {
  const attrSelect = document.getElementById('attr-select');
  ipcRenderer.send('ability roll',
    attrSelect.options[attrSelect.selectedIndex].innerHTML.toLowerCase(),
    abilSelect.options[abilSelect.selectedIndex].innerHTML.toLowerCase());
});