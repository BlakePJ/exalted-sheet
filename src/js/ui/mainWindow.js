const electron = require('electron');

const {ipcRenderer} = electron;

let button = document.getElementById('button');

button.addEventListener('click', () => {
  ipcRenderer.send('new char sheet');
});