const fs = require('fs');

function saveString(stream, bytes, id) {
  const element = document.getElementById(id);
  if(element === null) {
    console.log(`saveString(stream, ${bytes}, ${id}) failed because ` + 
      `document.getElementById('${id}') returned null`);
  } else {
    const contentBuf = Buffer.from(element.value);
    const byteBuf = Buffer.alloc(bytes);
    byteBuf.writeUIntLE(contentBuf.length, 0, bytes);
    stream.write(byteBuf);
    stream.write(contentBuf);
  }
}

function saveInt(stream, bytes, id) {
  const element = document.getElementById(id);
  if(element === null) {
    console.log(`saveInt(stream, ${bytes}, ${id}) failed because ` + 
      `document.getElementById('${id}') returned null`);
  } else {
    const buf = Buffer.alloc(bytes);
    buf.writeUIntLE(parseInt(element.value), 0, bytes);
    stream.write(buf);
  }
}

function saveDotsByte(stream, id) {
  const element = document.getElementById(id);
  if(element === null) {
    console.log(`saveDotsByte(stream, ${id}) failed because ` + 
      `document.getElementById('${id}') returned null`);
  } else {
    const attrDots = element.querySelectorAll('.checkbox');
    let byte = 0;
    for(let i = 0; i < attrDots.length; i++)
      if(attrDots[i].getElementsByTagName('input')[0].checked === true)
        byte |= 1 << i;
    const buf = Buffer.from([byte]);
    // console.log('saveDotsByte()\nbuf=', buf);
    stream.write(buf);
  }
}

function save(document) {
  const stream = fs.createWriteStream('test');

  saveString(stream, 1, 'char-name');
  saveString(stream, 1, 'caste-input');
  saveString(stream, 1, 'supernal-input');
  saveInt(stream, 2, 'current-xp-input');
  saveInt(stream, 2, 'total-xp-input');
  saveInt(stream, 2, 'current-solar-xp-input');
  saveInt(stream, 2, 'total-solar-xp-input');
  saveDotsByte(stream, 'strength-row');
  saveDotsByte(stream, 'dexterity-row');
  saveDotsByte(stream, 'stamina-row');
  saveDotsByte(stream, 'charisma-row');
  saveDotsByte(stream, 'manipulation-row');
  saveDotsByte(stream, 'appearance-row');
  saveDotsByte(stream, 'perception-row');
  saveDotsByte(stream, 'intelligence-row');
  saveDotsByte(stream, 'wits-row');
  saveDotsByte(stream, 'essence-row');
  saveInt(stream, 2, 'personal-essence-1');
  saveInt(stream, 2, 'personal-essence-2');
  saveInt(stream, 2, 'peripheral-essence-1');
  saveInt(stream, 2, 'peripheral-essence-2');
  saveInt(stream, 2, 'committed-essence');
  saveDotsByte(stream, 'archery-row');
  saveDotsByte(stream, 'athletics-row');
  saveDotsByte(stream, 'awareness-row');
  saveDotsByte(stream, 'brawl-row');
  saveDotsByte(stream, 'bureaucracy-row');
  saveDotsByte(stream, 'craft-row');
  saveDotsByte(stream, 'dodge-row');
  saveDotsByte(stream, 'integrity-row');
  saveDotsByte(stream, 'investigation-row');
  saveDotsByte(stream, 'larceny-row');
  saveDotsByte(stream, 'linguistics-row');
  saveDotsByte(stream, 'lore-row');
  

  stream.end();
  console.log('write completed');
}

function readVarLengthContent(buffer, offset, bytes) {
  let len;
  if(bytes === 1) {
    len = buffer.readUInt8(offset);
    offset += 1;
  } else if(bytes === 2) {
    len = buffer.readUInt16LE(offset);
    offset += 2;
  }

  const content = buffer.slice(offset, offset + len);
  offset += len;
  console.log(`Var: offset = ${offset}\ncontent = ${content}`);
  return [offset, content.toString()];
}

function readFixedLengthContent(buffer, offset, bytes) {
  if(bytes === 2) {
    const content = buffer.readUInt16LE(offset);
    offset += 2;
    console.log(`Fix: offset = ${offset}\ncontent = ${content}`);
    return [offset, content];
  }
}

function parseSaveFile(buffer) {
  let offset = 0;
  let content = '';
  [offset, content] = readVarLengthContent(buffer, offset, 1);
  [offset, content] = readVarLengthContent(buffer, offset, 1);
  [offset, content] = readVarLengthContent(buffer, offset, 1);
  [offset, content] = readFixedLengthContent(buffer, offset, 2);
}

function load() {
  fs.readFile('test', (err, data) => {
    if(err) console.log('Error: ', err);
    parseSaveFile(data);
  });
}

module.exports = {
  save,
  load
};