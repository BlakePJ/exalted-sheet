const electron = require('electron');
const url = require('url');
const path = require('path');
const windowStateKeeper = require('electron-window-state');

const {app, BrowserWindow, ipcMain, ipcRenderer} = electron;

// process.env.NODE_ENV = 'production';

let mainWindow;
let abilRollWindow;

app.on('ready', () => {
    let mainWindowState = windowStateKeeper({
        defaultWidth: 870,
        defaultHeight: 600
    });

    mainWindow = new BrowserWindow({
        x: mainWindowState.x,
        y: mainWindowState.y,
        width: mainWindowState.width,
        height: mainWindowState.height,
        title: 'Exalted Character Sheet',
        show: false,
        webPreferences: {
            nodeIntegration: true
        }
    });

    mainWindowState.manage(mainWindow);
    mainWindow.loadFile(path.join('src', 'html', 'mainWindow.html'));
    mainWindow.on('closed', () => {
        app.quit();
    });
    mainWindow.once('ready-to-show', () => {
        mainWindow.show();
    });
    // mainWindow.setMenu(null);

    abilRollWindow = new BrowserWindow({
        parent: mainWindow,
        modal: true,
        show: false,
        title: 'Ability Roll Window',
        webPreferences: {
            nodeIntegration: true
        }
    });
    abilRollWindow.loadFile(path.join('src', 'html', 'abilRollWindow.html'));
    abilRollWindow.on('close', (event) => {
        event.preventDefault();
        abilRollWindow.hide();
    });
    // abilRollWindow.setMenu(null);
});

ipcMain.on('new char sheet', () => {
    mainWindow.loadFile(path.join('src', 'html', 'charSheet.html'));
});

ipcMain.on('back', () => {
    mainWindow.loadFile(path.join('src', 'html', 'mainWindow.html'));
});

ipcMain.on('show ability roll window', (event, ability) => {
    abilRollWindow.show();
    abilRollWindow.webContents.send('change ability', ability);
    // console.log('ability = ' + ability);
});

ipcMain.on('ability roll', (event, attr, abil) => {
    mainWindow.webContents.send('ability roll', attr, abil);
});